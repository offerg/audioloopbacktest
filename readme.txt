Install guide:

download the PROCESSOR-SDK-RTOS-K2G from : http://www.ti.com/tool/processor-sdk-k2g

install the SDK in the default location.

build the SDK and the PDK library, following this instructions: http://software-dl.ti.com/processor-sdk-rtos/esd/docs/latest/rtos/Overview.html#building-the-sdk

Build the AUD addon following the next instruction: http://processors.wiki.ti.com/index.php/Processor_SDK_RTOS_AUDK2G_AddOn (note: there is no folder named "audk2g" the actual name is "aud")

download and install Code Composer Studio from : http://processors.wiki.ti.com/index.php/Download_CCS

clone the project from the GIT reposetry AudioLoopbackTest.


how to run the program:
open CCS.
open the AudioLoopbackTest procject by: file->Open Project From File system... .
build the project by: Project->Buid Project .
Ener the debug mode by: Run->Debug ,or by perssing F11.
Run the program by clicking in the Resume button, or by pressing F8.


Programming Guide:

DSP processing can be inserted in the function DspTask() that inside the mcasp_cfg.c file.

The buff size can be change in the mcasp_cfg.h file under BUFFLEN.

3 GPIO's can be modefied (0,1,2) using the functions GPIO_write(GPIO_num, GPIO_value) for writing to the GPIO, and GPIO_toggle(GPIO_num) for toggling the GPIO. the pins connected to the GPIO's are pins 19,21,23 in the Serial Expansion.